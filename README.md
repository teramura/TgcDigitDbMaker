# TgcDigitDbMaker
## How to make SQLite files 
- Setting up Athena and git clone
```
$ setupATLAS
$ asetup AtlasProduction 20.7.8.9
$ cd TgcDigitDbMaker/COOL
```
- Edit parameters in runL1TGCDataBase.py
```
input_    = 'TGC_Digitization_AsdPosition.dat'                 <- path to DIGIT DB files
output_   = 'TGC_Digitization_AsdPosition.db'                  <- name of SQLite file
instance_ = 'OFLP200'                                          <- OFLP200 for MC, CONDBR2 for run2 data
folder_   = '/TGC/DIGIT/ASDPOS'                                <- CW_{BW, EIFI, TILE} 
tag_      = 'TgcDigitAsdPos-00-01'                             <- name of tag folder
maxiov_   = cool.ValidityKeyMax                                <- no change required
iovs_     = [ [0,      maxiov_, 1, 'v01', 'full',   'TGC_Digitization_AsdPosition.dat'] ]
            <- [run since, run until, is active, version, type, file pattern], see the following section
```
- Run
```
$ python runL1TGCDataBase.py
```
- Edit checkSQLite.sh and check contents
```
$ ./checkSQLite.sh
```

## Contents of Database
- iov (run since and run until): range of runs, see [condition run numbers](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ConditionsRun1RunNumbers)
- active: 1 = coincidence on, 0 = otherwise
- version: version of Digit files
- type: type of Digit Param
- file: name of Digit files 
