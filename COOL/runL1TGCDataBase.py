#!/bin/env python

import sys, glob, os
from PyCool import cool
import pandas as pd

#---------------------------------------------------------------
# Parameters
#

input_    = 'TGC_Digitization_AsdPosition.dat'
output_   = 'TGC_Digitization_AsdPosition.db'
instance_ = 'OFLP200'
folder_   = '/TGC/DIGIT/ASDPOS'
tag_      = 'TgcDigitAsdPos-00-01'

#---------------------------------------------------------------
# Functions
#

def main(): 
    # remove the old db file so that we can write the new one
    os.system('rm ' + output_)

    # get database service and open database
    dbSvc = cool.DatabaseSvcFactory.databaseService()

    # database accessed via physical name
    dbString = "sqlite://;schema=%s;dbname=%s" % (output_, instance_)
    try:
        db = dbSvc.createDatabase(dbString)
    except Exception, e:
        print 'err> Problem creating database', e
        sys.exit(-1)
        print "inf> Created database", dbString
        
    # setup folder 
    spec = cool.RecordSpecification()
    spec.extend('stationNum',   cool.StorageType.UInt16)
    spec.extend('stationEta',   cool.StorageType.UInt16)
    spec.extend('stationPhi',   cool.StorageType.Int16)
    spec.extend('stripAsdPos1', cool.StorageType.Float)
    spec.extend('stripAsdPos2', cool.StorageType.Float)
    spec.extend('wireAsdPos1',  cool.StorageType.Float)
    spec.extend('wireAsdPos2',  cool.StorageType.Float)
    spec.extend('wireAsdPos3',  cool.StorageType.Float)
    spec.extend('wireAsdPos4',  cool.StorageType.Float)
    spec.extend('wireAsdPos5',  cool.StorageType.Float)
    spec.extend('wireAsdPos6',  cool.StorageType.Float)
    spec.extend('wireAsdPos7',  cool.StorageType.Float)
    spec.extend('wireAsdPos8',  cool.StorageType.Float)

    # folder meta-data - note for Athena this has a special meaning
    desc  = '<timeStamp>run-lumi</timeStamp>'
    desc += '<addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader>'
    desc += '<typeName>CondAttrListCollection</typeName>'
    
    # create the folder - multiversion version
    folderSpec = cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, spec)
    folder = db.createFolder(folder_, folderSpec, desc, True)
    
    # now fill in some data - create a record and fill it
    data = cool.Record(spec)

    tgcData = pd.read_table(input_,header=None)
    txtlines = 62
    for txtline in range(txtlines):
        data['stationNum']   = tgcData[0][txtline]
        data['stationEta']   = tgcData[1][txtline]
        data['stationPhi']   = tgcData[2][txtline]
        data['stripAsdPos1'] = tgcData[3][txtline] 
        data['stripAsdPos2'] = tgcData[4][txtline]
        data['wireAsdPos1']  = tgcData[5][txtline] 
        data['wireAsdPos2']  = tgcData[6][txtline]
        data['wireAsdPos3']  = tgcData[7][txtline] 
        data['wireAsdPos4']  = tgcData[8][txtline]
        data['wireAsdPos5']  = tgcData[9][txtline] 
        data['wireAsdPos6']  = tgcData[10][txtline]
        data['wireAsdPos7']  = tgcData[11][txtline] 
        data['wireAsdPos8']  = tgcData[12][txtline]

        folder.storeObject(0, cool.ValidityKeyMax, data, txtline, tag_)
           
    print "inf> Close database"
    db.closeDatabase()

if __name__=="__main__":
    main()
